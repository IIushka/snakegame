// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "Food.h"
#include <ctime>


// Sets default values
ASpawner::ASpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

	SpawnObject();
}

void ASpawner::SpawnObject()
{
	do
	{
		int32 XMin = -500;
		int32 XMax = 500;
		int32 YMin = -1000;
		int32 YMax = 1000;
		int32 x, y;

		srand(static_cast<int>(time(NULL)));

		if (XMin > 0)
			x = XMin + rand() % (XMax - XMin);
		else
			x = XMin + rand() % (abs(XMin) + XMax);

		if (YMin > 0)
			y = YMin + rand() % (YMax - YMin);
		else
			y = YMin + rand() % (abs(YMin) + YMax);

		FVector NewLocation(x, y, 0);
		FTransform NewTransform(NewLocation);
		NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	} while (NewFood == nullptr);

	NewFood->InitFoodSpawner(this);
}


// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

